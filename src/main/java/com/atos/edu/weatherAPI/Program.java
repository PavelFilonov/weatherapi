package com.atos.edu.weatherAPI;

import com.jayway.jsonpath.JsonPath;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.json.JSONObject;

import java.io.IOException;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.Scanner;

public class Program {

    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Input your city: ");
        String city = scanner.next();
        scanner.close();

        try {
            double temp = getTempByCity(city, "7b6be55ecfc023f52792505653e8e278");
            System.out.println(String.format("Temp in the %s: " + temp + " C", city));
        } catch (IOException e) {
            System.err.println("IOException\n" + e.getMessage());
        }
    }

    private static double getTempByCity(String city, String apiKey) throws IOException {
        String url = String.format("https://api.openweathermap.org/data/2.5/weather?q=%s&appid=%s", city, apiKey);

        JSONObject responseJson = getResponseJson(url);

        double lat = JsonPath.read(responseJson.toString(), "$.coord.lat");
        double lon = JsonPath.read(responseJson.toString(), "$.coord.lon");

        return getTempByCoordinate(lat, lon, apiKey);
    }

    private static double getTempByCoordinate(double lat, double lon, String apiKey) throws IOException {
        String url = String.format("https://api.openweathermap.org/data/2.5/onecall?lat=%s&lon=%s&appid=%s",
                lat, lon, apiKey);

        JSONObject responseJson = getResponseJson(url);

        double temp = JsonPath.read(responseJson.toString(), "$.current.temp");
        int dateUnixTimeSeconds = JsonPath.read(responseJson.toString(), "$.current.dt");

        String timeZone = JsonPath.read(responseJson.toString(), "$.timezone");
        LocalDateTime localDateTime = LocalDateTime.ofEpochSecond(
                dateUnixTimeSeconds,
                1,
                ZoneOffset.of(ZoneId.of(timeZone).getRules().toString().split("=")[1].split("]")[0])
        );
        System.out.println("Weather date: " + localDateTime.toString().split("\\.")[0]);

        return Math.round(kelvinToCelsius(temp));
    }

    private static JSONObject getResponseJson(String url) throws IOException {
        CloseableHttpClient httpClient = HttpClients.createDefault();
        HttpGet httpGet = new HttpGet(url);
        CloseableHttpResponse httpResponse = httpClient.execute(httpGet);
        String responseString = EntityUtils.toString(httpResponse.getEntity(), "UTF-8");

        httpClient.close();
        httpResponse.close();

        return new JSONObject(responseString);
    }

    private static double kelvinToCelsius(double kelvinDegrees) {
        return kelvinDegrees - 273.15;
    }
}
